#!/usr/bin/env bash

# Initialize counters to 0 in hash --
v, c, d = 0

# For each word (arg) on the command line, separated by spaces --
for each word in $*

    # Count based on first-letter of each word --
    case xxx

    esac

# Report the counts --
echo "Count of words beginning with a consonant: ${c}"
echo "Count of words beginning with a vowel: ${v}"
echo "Count of words beginning with a digit: ${d}"
