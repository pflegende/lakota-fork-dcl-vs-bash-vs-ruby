#!/usr/bin/env ruby

# Invoked on command line as:
#  $ ruby countnames.rb < names.lis

# Initialize a names hash (dictionary) as empty to start with.
# Each key in this dictionary will be a name and the value
# will be the number of times that name appears.

# names = {} ...But this is better, providing a 0 default
#               value for first-time incrementing as
#               keys (names) are inserted:
names = Hash.new( 0 )

# $stdin is a file object consisting of the concatenation of all files
# appearing on the command-line (as StdIn).  All the same functions
# that can be applied to a file object can be applied to $stdin.

# Each line will have a newline on the end that should be removed.
$stdin.readlines.each { |name| names[name.chop] += 1 }

# Iterating over the dictionary, print name followed by a tab
# followed by the number of times it appeared.

names.each { |name,count| puts "#{name}\t#{count}" }

exit true
